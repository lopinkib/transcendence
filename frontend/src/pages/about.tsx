import Navbar from "@/components/Navbar";

const about = () => {
	return (
		<div className="">
			<Navbar />
			<div className="page" style={{margin:100}}>
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis explicabo, dolores, enim voluptatem quaerat delectus vitae doloribus et dignissimos ipsum fuga soluta quasi quibusdam. Neque saepe molestias ipsa voluptatibus sunt.
				Eos esse explicabo cum molestiae iusto similique, perferendis aliquid voluptatem in accusamus laudantium. Illum facere maiores omnis atque, praesentium sit tempore numquam deserunt voluptatem quo eius, quae dolor repellat nam.
				Fuga nobis totam voluptatibus aliquid corporis? Dolor quas quis pariatur dignissimos vero vel similique, esse eaque quos natus culpa officiis in ea omnis ipsum quam doloribus quod nobis? Explicabo, assumenda.
				Maiores debitis quas consequatur dolorum laborum, praesentium explicabo inventore ipsam. Nesciunt, maxime dicta nobis dolorum commodi molestiae quisquam ut a sapiente, quas iste recusandae perspiciatis magnam dolor doloremque rerum facilis.
				Veniam molestiae itaque autem quos beatae cumque reiciendis ut omnis odio libero, minus voluptatum delectus corporis quis molestias accusantium magni impedit laboriosam reprehenderit. Aliquam porro, inventore ex ab blanditiis corrupti.
				Libero voluptatibus quaerat animi quae excepturi accusantium consequuntur fugiat autem iusto nostrum architecto vitae totam obcaecati atque voluptas assumenda corrupti, quidem adipisci qui illum. Vero architecto distinctio perferendis nesciunt aut!
				Accusamus expedita incidunt voluptas dolorum quia magni obcaecati placeat, ad minus eos minima beatae, assumenda consectetur! Architecto suscipit est repellendus facere. Cumque excepturi commodi sit non vel, ratione labore nihil.
				A officiis, excepturi saepe quo, cum voluptatum adipisci omnis assumenda quibusdam accusantium voluptatibus repellat pariatur cumque sunt ipsum numquam. Culpa cumque nihil fugiat qui nesciunt dolore aliquam nemo aperiam harum.
				Aperiam, ipsum! Nam iure illum sed unde laboriosam impedit debitis harum? Voluptate, repellat ut beatae itaque quisquam deserunt. Asperiores temporibus labore voluptate nihil ipsum quos a delectus iure tempora! Quidem?
				Officia quae quis ut, sapiente et cupiditate voluptatum atque non? In eaque soluta, earum, modi illum iure nemo aliquid ipsum placeat amet repellat eligendi laudantium obcaecati nostrum adipisci est natus.
				Nostrum autem ipsum, deleniti commodi necessitatibus velit fuga officia facilis voluptates laborum voluptate magnam blanditiis at explicabo repellendus beatae voluptatum est. Iusto suscipit sit doloribus fugit, deleniti reprehenderit magni dolorem?
				Eligendi voluptatem deleniti, expedita quo, consequatur, corrupti quis laborum est inventore sit non ducimus necessitatibus voluptatum. Voluptate eveniet unde officiis accusamus, a facilis dolor pariatur, totam eos nihil cum quia.
				Repellat quos maiores tempora ipsum iusto quisquam, accusantium, quam sint perferendis dolores odit magni adipisci, modi nostrum eveniet beatae exercitationem porro a! Nulla officiis sint natus repudiandae eveniet inventore consequuntur.
				Mollitia ab, reiciendis natus ipsa nulla sequi eaque adipisci cumque sed. Dolorum dolor cum ad repellendus. Dolore iure fugit laborum! Suscipit esse sint doloremque pariatur. Repellendus unde quidem nam ipsam!
				Ab dicta sunt id deserunt incidunt accusantium deleniti similique omnis temporibus repellat voluptatibus assumenda, exercitationem labore iusto eius rem dolor earum at ex totam? Quisquam impedit aliquam fugit laudantium nesciunt!
				Corrupti, consequatur vero. Ullam adipisci suscipit recusandae repellendus fugiat quod veritatis libero nulla ut a distinctio odio provident nihil est, eveniet deserunt aliquid, modi architecto, voluptatibus cumque illo doloremque dolorem!
				Vitae voluptate iusto omnis, inventore unde assumenda esse maiores consequuntur commodi dignissimos amet rerum corrupti asperiores deleniti veritatis? Quis, repellendus illum cupiditate placeat obcaecati inventore rerum facilis fugit illo ex.
				Illo esse consequuntur ipsam voluptates repellat dolores sequi et. Provident, dolorum consectetur. Quas dolorum nobis quibusdam distinctio necessitatibus animi nihil reiciendis autem facilis ipsam, eveniet sunt delectus quia dolor repellendus.
				Sapiente repellendus sunt vero architecto temporibus ad, delectus earum, voluptate impedit similique officiis non minima ratione voluptas ab omnis ipsam enim beatae itaque assumenda nemo tenetur, incidunt corporis nesciunt. Incidunt.
				Quae porro, dolore illo laborum, architecto provident magnam debitis possimus ipsam praesentium beatae! Itaque, enim? Aperiam tempora laudantium exercitationem iure fugit? Id dignissimos recusandae, ea excepturi ipsa amet cupiditate magnam.
			</div>
		</div>
	);
}

export default about