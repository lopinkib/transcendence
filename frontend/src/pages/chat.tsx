import Navbar from '@/components/Navbar'
import '../styles/globals.css'

export default function ChatPage() {
	return (
		<div style={{height: '100vh'}}>
			<Navbar />
			<main>Here will be the chat</main>
		</div>
	)
}
